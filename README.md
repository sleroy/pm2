Pm2 is a command line software, c++ library, and [Audiosculpt](https://forum.ircam.fr/projects/detail/audiosculpt/)
processing kernel for sound analysis synthesis based on the sinusoidal signal model. It integrates state of the art algorithms for partial analysis that improve notably the estimation errors for the analysis of non sationary sinusoids.

## Design and Development ##
[Audiosculpt](https://forum.ircam.fr/projects/detail/audiosculpt/), SuperVP, Pm2 and [IrcamBeat](http://www.quaero.org/module_technologique/ircambeat-music-tempo-meter-beat-and-downbeat-estimation/) are developed by the [Sound Analysis-Synthesis team](https://www.ircam.fr/recherche/equipes-recherche/anasyn/) at IRCAM.
